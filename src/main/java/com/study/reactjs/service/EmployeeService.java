package com.study.reactjs.service;

import com.study.reactjs.entity.Employee;
import com.study.reactjs.repository.EmployeeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    public Employee createEmployee(Employee employee) {
        return this.employeeRepository.save(employee);
    }

    public Employee editEmployee(Employee employee) {
        return this.employeeRepository.save(employee);
    }
}
